# adminportal

# Portal administrator

This project was started with Create React App.

## Description

The admin portal is a web application allowing the revocation and suspension of VCs issued by ICP (Issuance Credential Protocol).

## Operation of the portal

The portal allows the administrator to manage VCs issued by ICP (Issuance Credential Protocol). Here is the working flow:

1. The administrator launches the portal and is redirected to a welcome page containing a login button.
2. When he clicks on the button, he is redirected to the Back Office (eg Keycloak) to enter his authentication information.
3. Once authenticated, the backoffice redirects to the VC management page.
4. From this page, the administrator has access to VC management features.

## List of Portal Features

1. Display of all VCs issued via ICP.
2. Display the total list of issued CVs.
3. View revocation list.
4. Display a given VC.
5. Check if a VC has been revoked or suspended.
6. Revoke a VC.
7. Suspend a given VC.
8. Reactivate a suspended VC.

## Components involved in operation

- The web portal: [Link to the GitLab repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/adminportal.git)
- The BackOffice: Keycloak [Link to the Keycloak instance](https://keycloak.abc-federation.dev.gaiax.ovh/)
- ICP (Issuance Credential Protocol): [Link to GitLab repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/tree/ hand/)
- The Registry: [Link to the GitLab repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry)
- Database: MongoDB

## Routes implemented in ICP for portal management

| HTTP Verb | URL                                       | Input                        | Output                                           | Description                                      |
|-----------|-------------------------------------------|------------------------------|--------------------------------------------------|--------------------------------------------------|
| GET       | https://{service}/api/users/listVC         | n/a                          | A list of Credentials                             |Retrieval of all VCs issued via ICP    |
| POST      | https://{service}/api/users/revokeVC       | The credential status       | Code 200                                         | Revoke a VC                                  |
| POST      | https://{service}/api/users/checkVC        | The credential status       | {suspended: true or false, revoked: true or false} |Check if a given VC has been revoked or suspended |
| GET       | https://{service}/api/users/showListrevokeVC | n/a                          | A list of Credentials                             | Revocation list retrieval            |
| POST      | https://{service}/api/users/suspenseVC     | The credential status       | Code 200                                         | Suspend a given VC                            |
| POST      | https://{service}/api/users/reactivateVC   | The credential status       | Code 200                                         | Reactivate a suspended VC                          |

Please replace `{service}` in the URLs with the actual URL of the ICP service used in your admin portal.

## Getting Started

To get started with this project, follow the steps below:

1. Clone the repository.
2. Install the dependencies by running `npm install` in the project directory.
3. Start the development server with `npm start`.
4. Open [http://localhost:3000](http://localhost:3000) in your browser to view the application.

## Installation

1. Install Node.js: ReactJS requires Node.js, so be sure to install it on your machine. You can download it from Node.js official website and follow the installation instructions.

2. Clone the project locally: Run the following command to clone the project to your machine: `git clone` [ https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential -issuer/adminportal.git](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/adminportal.git)

3. Navigate to the project directory after project cloning is complete.
4. Run the `npm install` command to install the project dependencies.
5. Launch the React application: To launch your React application, run the following command: `npm start` at the root of our project to launch the portal.

## Technologies used

The technologies used in this project are:

1. ReactJS: A JavaScript library for building responsive user interfaces.
2. Node.js: A server-side JavaScript runtime environment for building web applications.
3. Material-UI: A React component library based on Material design for an attractive user interface.
4. CSS: for customizing application styles.
5. Express.js: A minimalist web framework for building server-side RESTful APIs.
6. MongoDB: a NoSQL database for storing and retrieving data.



